package main

import (
	"fmt"
	"flag"
	"moneyconv/converter"
)

func main() {
	var amount, rate, result int
	flag.IntVar(&amount, "amount", 0, "Сумма для перевода")
	flag.IntVar(&rate, "rate", 0, "Курс")
	flag.Parse()
	result = converter.Converter(rate, amount)
	fmt.Println(` result `)
	fmt.Println(result)

}
