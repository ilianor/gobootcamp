package main

import (
    "fmt"
    bowlingPlayer "gamer/player/bowlPlayer"
)

func main() {
    player := bowlingPlayer.Player{
        Name: bowlingPlayer.Names{
            FirstName: "Boris",
            LastName: "Britwa",
        },
        Date: bowlingPlayer.BirthDate{
            Year: 1954,
            Month: 4,
            Day: 12,
        },
        PartyNumber: 12,
        SummaryScore: 345,
    }


    fmt.Printf("Cчет игрока %f\n", player.GetPlayerMiddleScore())
    fmt.Printf("Возраст is %d\n", player.GetPlayerAge())
}