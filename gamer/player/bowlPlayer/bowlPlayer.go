package bowlPlayer

import "time"

type Player struct {
    Name Names
    Date BirthDate
    PartyNumber int
    SummaryScore float32
}
type Names struct {
    FirstName string
    LastName string
}

type BirthDate struct {
    Year int
    Month int
    Day int
}

func (player Player) GetPlayerMiddleScore() float32 {
    return player.SummaryScore / float32(player.PartyNumber)
}

func (player Player) GetPlayerAge() int64 {
    t := time.Date(
        player.Date.Year,
        time.Month(player.Date.Month),
        player.Date.Day,
        0,
        0,
        0,
        0,
        time.FixedZone("MSK", 3 * 60 * 60),
    )
    diff := time.Now().Unix() - t.Unix()
    return diff / (365 * 24 * 60 * 60)
}
