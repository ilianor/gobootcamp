package hr

type Employee struct {
	Name    string
	Age     int
	Cash    float64
	Holiday bool
}

func EmployeeCreator(name string, age int, cash float64, holiday bool) struct {
	Name    string
	Age     int
	Cash    float64
	Holiday bool
} {
	return Employee{
		Name:    name,
		Age:     age,
		Cash:    cash,
		Holiday: holiday,
	}
}
