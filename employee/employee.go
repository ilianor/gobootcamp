package main

import (
	"employee/hr"
	"flag"
	"fmt"
)

func main() {
	t := struct {
		Name    string
		Age     int
		Cash    float64
		Holiday bool
	}{}

	flag.StringVar(&t.Name, "name", "", "Имя сотрудника")
	flag.IntVar(&t.Age, "age", 0, "Возраст сотрудника")
	flag.Float64Var(&t.Cash, "cash", 0, "Зарплата сотрудника")
	flag.BoolVar(&t.Holiday, "holiday", false, "В отпуске ли сотрудник сотрудника")

	flag.Parse()
	var e = hr.EmployeeCreator(t.Name, t.Age, t.Cash, t.Holiday)

	fmt.Printf("%s %d", e.Name, e.Age, e.Cash, e.Holiday)
}
